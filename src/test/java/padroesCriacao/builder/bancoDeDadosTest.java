package padroesCriacao.builder;

import org.junit.jupiter.api.Test;
import padroesCriacao.builder.data.BancoDeDados;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.*;

public class bancoDeDadosTest {
    @Test
    public void casoBancoEstejaDesligadoRetornaErro(){
        Connection connection = BancoDeDados.conectar();
        if (connection == null){
            assertNull(connection);
            fail();
        }else {
            assertNotNull(connection);
        }
    }
}
