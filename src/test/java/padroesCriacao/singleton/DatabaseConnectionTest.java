package padroesCriacao.singleton;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import padroesCriacao.singleton.DatabaseConnection;

public class DatabaseConnectionTest {
    private DatabaseConnection databaseConnection;

    @Before
    public void setUp() throws SQLException {
        databaseConnection = DatabaseConnection.getInstance();
    }
    @Test
    public void deveRetornarMesmaInstancia() throws SQLException {
        DatabaseConnection anotherDatabaseConnection;
        anotherDatabaseConnection = DatabaseConnection.getInstance();
        assertSame(databaseConnection, anotherDatabaseConnection);
    }

    @Test
    public void deveRetornarConexaoNaoNula() {
        Connection connection = databaseConnection.getConnection();
        assertNotNull(connection);
    }

    @After
    public void tearDown() throws SQLException {
        databaseConnection.closeConnection();
    }
}
