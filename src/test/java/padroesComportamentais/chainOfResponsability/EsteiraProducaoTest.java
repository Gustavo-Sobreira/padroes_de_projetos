package padroesComportamentais.chainOfResponsability;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EsteiraProducaoTest {

    EsteiraProducao qualidade;
    EsteiraProducao design;
    EsteiraProducao desenvolvimento;
    EsteiraProducao analise;

    @BeforeEach
    void setUp() {
        analise = new SetorAnalise(null);
        desenvolvimento = new SetorDesenvolvimento(analise);
        qualidade = new SetorQualidade(desenvolvimento);
    }

    @Test
    void deveRetornarAnaliseParaTarefaDeAnalise() {
        assertEquals("Análise", qualidade.atribuirTarefa(new Tarefa(TipoTarefaAnalisarDemanda.getTipoTarefa())));
    }

    @Test
    void deveRetornarDesenvolvimentoParaTarefaDeDesenvolvimento() {
        assertEquals("Desenvolvimento", qualidade.atribuirTarefa(new Tarefa(TipoTarefaDesenvolverCodigo.getTipoTarefa())));
    }

    @Test
    void deveRetornarTesteParaTarefaDeQualidade() {
        assertEquals("Teste", qualidade.atribuirTarefa(new Tarefa(TipoTarefaTestarCodigo.getTipoTarefa())));
    }

    @Test
    void  deveRetornarNenhumSetorParaSetorDInvalidoesign() {
        assertEquals("Nenhum setor encontrado", qualidade.atribuirTarefa(new Tarefa(TipoTarefaCriarDesign.getTipoTarefa())));
    }

    @Test
    void deveRetornarNenhumSetorParaTarefaInvalida() {
        assertEquals("Nenhum setor encontrado", qualidade.atribuirTarefa(new Tarefa(new TipoTarefa() {})));
    }
}
