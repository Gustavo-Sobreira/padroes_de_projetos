package padroesComportamentais.visitor;

import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertEquals;

public class VisitorTest {

    @Test
    public void testCalculadoraSalarioLiquidoVisitor() {
        // Criação de objetos de funcionário para teste
        FuncionarioPermanente permanente = new FuncionarioPermanente("João", 2000);
        FuncionarioTemporario temporario = new FuncionarioTemporario("Maria", 40, 50);
        Estagiario estagiario = new Estagiario("Pedro", 20);

        // Instanciação das calculadoras
        CalculadoraSalarioLiquidoVisitor salarioVisitor = new CalculadoraSalarioLiquidoVisitor();

        // Redireciona a saída padrão para capturar a impressão no console
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Chamadas aos métodos de visitar
        permanente.aceitar(salarioVisitor);
        temporario.aceitar(salarioVisitor);
        estagiario.aceitar(salarioVisitor);

        // Restaura a saída padrão
        System.setOut(System.out);

        // Verifica os resultados
        assertEquals("Salário líquido de João: 2000.0\nSalário líquido de Maria: 2000.0\nSalário líquido de Pedro: 0.0", outContent.toString().trim());
    }

    @Test
    public void testCalculadoraHorasTrabalhadasVisitor() {
        // Criação de objetos de funcionário para teste
        FuncionarioPermanente permanente = new FuncionarioPermanente("João", 2000);
        FuncionarioTemporario temporario = new FuncionarioTemporario("Maria", 40, 50);
        Estagiario estagiario = new Estagiario("Pedro", 20);

        // Instanciação das calculadoras
        CalculadoraHorasTrabalhadasVisitor horasVisitor = new CalculadoraHorasTrabalhadasVisitor();

        // Redireciona a saída padrão para capturar a impressão no console
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Chamadas aos métodos de visitar
        permanente.aceitar(horasVisitor);
        temporario.aceitar(horasVisitor);
        estagiario.aceitar(horasVisitor);

        // Restaura a saída padrão
        System.setOut(System.out);

        // Verifica os resultados
        assertEquals("Horas trabalhadas de João: 40 horas\nHoras trabalhadas de Maria: 40 horas\nHoras trabalhadas de Pedro: 20 horas", outContent.toString().trim());
    }
}
