package padroesComportamentais.observer;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class UsuarioTest {

    private Usuario usuario;

    @Before
    public void setUp() {
        usuario = new Usuario("Alice");
    }

    @Test
    public void testNovaPostagem() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Criando seguidores
        Seguidor seguidor1 = new Seguidor("Follower 1");
        Seguidor seguidor2 = new Seguidor("Follower 2");

        // Adicionando seguidores ao usuário
        usuario.adicionarSeguidor(seguidor1);
        usuario.adicionarSeguidor(seguidor2);

        // Fazendo uma nova postagem
        usuario.novaPostagem("Olá, mundo!");

        // Verificando se os seguidores receberam a notificação
        String expectedOutput = "Follower 1 recebeu uma notificação: Alice: Olá, mundo!\n" +
                "Follower 2 recebeu uma notificação: Alice: Olá, mundo!\n";
        assertEquals(expectedOutput, outContent.toString());
    }
}
