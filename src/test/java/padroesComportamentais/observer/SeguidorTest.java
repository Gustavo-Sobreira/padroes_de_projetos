package padroesComportamentais.observer;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SeguidorTest {

    private Seguidor seguidor;

    @Before
    public void setUp() {
        seguidor = new Seguidor("Follower");
    }

    @Test
    public void testReceberNotificacao() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        seguidor.receberNotificacao("Alice: Olá, mundo!");
        assertEquals("Follower recebeu uma notificação: Alice: Olá, mundo!\n", outContent.toString());
    }
}
