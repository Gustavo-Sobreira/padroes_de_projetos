package padroesComportamentais.strategy;

import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertEquals;

public class ProcessadorPagamentoTest {

    private ProcessadorPagamento processador;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        processador = new ProcessadorPagamento();
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testProcessarPagamentoCartaoCredito() {
        processador.setEstrategia(new PagamentoCartaoCredito());
        processador.processarPagamento(100.0);
        assertEquals("Pagamento de R$100.0 realizado com cartão de crédito.\n", outContent.toString());
    }

    @Test
    public void testProcessarPagamentoPayPal() {
        processador.setEstrategia(new PagamentoPayPal());
        processador.processarPagamento(50.0);
        assertEquals("Pagamento de R$50.0 realizado com PayPal.\n", outContent.toString());
    }

    @Test
    public void testProcessarPagamentoTransferenciaBancaria() {
        processador.setEstrategia(new PagamentoTransferenciaBancaria());
        processador.processarPagamento(200.0);
        assertEquals("Pagamento de R$200.0 realizado por transferência bancária.\n", outContent.toString());
    }
}
