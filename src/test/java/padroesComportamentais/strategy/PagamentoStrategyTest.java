package padroesComportamentais.strategy;

import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertEquals;

public class PagamentoStrategyTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test
    public void testPagamentoCartaoCredito() {
        System.setOut(new PrintStream(outContent));
        PagamentoStrategy pagamento = new PagamentoCartaoCredito();
        pagamento.processarPagamento(100.0);
        assertEquals("Pagamento de R$100.0 realizado com cartão de crédito.\n", outContent.toString());
    }

    @Test
    public void testPagamentoPayPal() {
        System.setOut(new PrintStream(outContent));
        PagamentoStrategy pagamento = new PagamentoPayPal();
        pagamento.processarPagamento(50.0);
        assertEquals("Pagamento de R$50.0 realizado com PayPal.\n", outContent.toString());
    }

    @Test
    public void testPagamentoTransferenciaBancaria() {
        System.setOut(new PrintStream(outContent));
        PagamentoStrategy pagamento = new PagamentoTransferenciaBancaria();
        pagamento.processarPagamento(200.0);
        assertEquals("Pagamento de R$200.0 realizado por transferência bancária.\n", outContent.toString());
    }
}
