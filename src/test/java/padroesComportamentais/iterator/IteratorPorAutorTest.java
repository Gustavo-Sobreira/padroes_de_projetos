package padroesComportamentais.iterator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class IteratorPorAutorTest {

    private List<Livro> livros;

    @Before
    public void setUp() {
        livros = new ArrayList<>();
        livros.add(new Livro("Livro A", "Autor 1", 2020));
        livros.add(new Livro("Livro B", "Autor 2", 2021));
        livros.add(new Livro("Livro C", "Autor 1", 2022));
    }

    @Test
    public void testIteratorPorAutor() {
        Iterator iterator = new IteratorPorAutor(livros, "Autor 1");
        assertTrue(iterator.hasNext());
        assertEquals("Livro A", iterator.next().getTitulo());
        assertTrue(iterator.hasNext());
        assertEquals("Livro C", iterator.next().getTitulo());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testIteratorPorAutorInexistente() {
        Iterator iterator = new IteratorPorAutor(livros, "Autor Inexistente");
        assertFalse(iterator.hasNext());
    }
}
