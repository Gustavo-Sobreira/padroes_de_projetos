package padroesComportamentais.iterator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BibliotecaTest {

    private Biblioteca biblioteca;

    @Before
    public void setUp() {
        biblioteca = new Biblioteca();
        biblioteca.adicionarLivro(new Livro("Livro A", "Autor 1", 2020));
        biblioteca.adicionarLivro(new Livro("Livro B", "Autor 2", 2021));
        biblioteca.adicionarLivro(new Livro("Livro C", "Autor 1", 2022));
    }

    @Test
    public void testCriarIteratorTodosLivros() {
        Iterator iterator = biblioteca.criarIteratorTodosLivros();
        assertTrue(iterator.hasNext());
        assertEquals("Livro A", iterator.next().getTitulo());
        assertTrue(iterator.hasNext());
        assertEquals("Livro B", iterator.next().getTitulo());
        assertTrue(iterator.hasNext());
        assertEquals("Livro C", iterator.next().getTitulo());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testCriarIteratorPorAutor() {
        Iterator iterator = biblioteca.criarIteratorPorAutor("Autor 1");
        assertTrue(iterator.hasNext());
        assertEquals("Livro A", iterator.next().getTitulo());
        assertTrue(iterator.hasNext());
        assertEquals("Livro C", iterator.next().getTitulo());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testCriarIteratorPorAno() {
        Iterator iterator = biblioteca.criarIteratorPorAno(2020);
        assertTrue(iterator.hasNext());
        assertEquals("Livro A", iterator.next().getTitulo());
        assertTrue(iterator.hasNext());
        assertEquals("Livro C", iterator.next().getTitulo());
        assertFalse(iterator.hasNext());
    }
}

