package padroesComportamentais.iterator;

import org.junit.Test;
import static org.junit.Assert.*;

public class LivroTest {

    @Test
    public void testLivro() {
        Livro livro = new Livro("Título", "Autor", 2021);
        assertEquals("Título", livro.getTitulo());
        assertEquals("Autor", livro.getAutor());
        assertEquals(2021, livro.getAno());
    }

    @Test
    public void testToString() {
        Livro livro = new Livro("Título", "Autor", 2021);
        String expected = "Livro{titulo='Título', autor='Autor', ano=2021}";
        assertEquals(expected, livro.toString());
    }
}
