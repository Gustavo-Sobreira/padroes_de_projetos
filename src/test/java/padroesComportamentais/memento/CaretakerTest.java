package padroesComportamentais.memento;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CaretakerTest {

    private Caretaker caretaker;

    @Before
    public void setUp() {
        caretaker = new Caretaker();
    }

    @Test
    public void testSalvarEstado() {
        Memento memento = new Memento("Escuro", "Arial", "Azul");
        caretaker.salvarEstado(memento);
        assertEquals(memento, caretaker.restaurarEstado());
    }

    @Test
    public void testRestaurarEstado() {
        Memento memento1 = new Memento("Escuro", "Arial", "Azul");
        Memento memento2 = new Memento("Claro", "Times New Roman", "Vermelho");
        caretaker.salvarEstado(memento1);
        caretaker.salvarEstado(memento2);
        assertEquals(memento2, caretaker.restaurarEstado());
        assertEquals(memento1, caretaker.restaurarEstado());
    }

    @Test
    public void testRestaurarEstadoSemSalvar() {
        assertEquals(null, caretaker.restaurarEstado());
    }
}
