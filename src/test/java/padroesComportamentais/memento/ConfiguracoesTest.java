package padroesComportamentais.memento;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConfiguracoesTest {

    private Configuracoes configuracoes;

    @Before
    public void setUp() {
        configuracoes = new Configuracoes();
    }

    @Test
    public void testPersonalizar() {
        configuracoes.personalizar("Escuro", "Arial", "Azul");
        assertEquals("Escuro", configuracoes.getTemaAtual());
        assertEquals("Arial", configuracoes.getFonteAtual());
        assertEquals("Azul", configuracoes.getCorAtual());
    }

    @Test
    public void testDesfazer() {
        configuracoes.personalizar("Claro", "Times New Roman", "Vermelho");
        configuracoes.desfazer();
        assertEquals(null, configuracoes.getTemaAtual());
        assertEquals(null, configuracoes.getFonteAtual());
        assertEquals(null, configuracoes.getCorAtual());
    }

    @Test
    public void testDesfazerDuasVezes() {
        configuracoes.personalizar("Claro", "Times New Roman", "Vermelho");
        configuracoes.desfazer();
        configuracoes.desfazer();
        assertEquals(null, configuracoes.getTemaAtual());
        assertEquals(null, configuracoes.getFonteAtual());
        assertEquals(null, configuracoes.getCorAtual());
    }

    @Test
    public void testDesfazerSemAlteracoes() {
        configuracoes.desfazer();
        assertEquals(null, configuracoes.getTemaAtual());
        assertEquals(null, configuracoes.getFonteAtual());
        assertEquals(null, configuracoes.getCorAtual());
    }
}
