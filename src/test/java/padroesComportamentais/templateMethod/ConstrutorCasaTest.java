package padroesComportamentais.templateMethod;

import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertEquals;

public class ConstrutorCasaTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test
    public void testConstrutorCasaMadeira() {
        System.setOut(new PrintStream(outContent));
        ConstrutorCasa construtorMadeira = new ConstrutorCasaMadeira();
        construtorMadeira.construirCasa();
        assertEquals("Construindo estrutura de madeira\n" +
                "Adicionando paredes de madeira\n" +
                "Adicionando telhado de madeira\n" +
                "Adicionando portas e janelas de madeira\n" +
                "Decorando casa de madeira\n", outContent.toString());
    }

    @Test
    public void testConstrutorCasaAlvenaria() {
        System.setOut(new PrintStream(outContent));
        ConstrutorCasa construtorAlvenaria = new ConstrutorCasaAlvenaria();
        construtorAlvenaria.construirCasa();
        assertEquals("Construindo estrutura de alvenaria\n" +
                "Adicionando paredes de alvenaria\n" +
                "Adicionando telhado de alvenaria\n" +
                "Adicionando portas e janelas de alvenaria\n" +
                "Decorando casa de alvenaria\n", outContent.toString());
    }

    @Test
    public void testConstrutorCasaPreFabricada() {
        System.setOut(new PrintStream(outContent));
        ConstrutorCasa construtorPreFabricada = new ConstrutorCasaPreFabricada();
        construtorPreFabricada.construirCasa();
        assertEquals("Construindo estrutura pré-fabricada\n" +
                "Adicionando paredes pré-fabricadas\n" +
                "Adicionando telhado pré-fabricado\n" +
                "Adicionando portas e janelas pré-fabricadas\n" +
                "Decorando casa pré-fabricada\n", outContent.toString());
    }
}
