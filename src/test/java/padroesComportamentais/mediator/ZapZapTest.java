package padroesComportamentais.mediator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ZapZapTest {

    private ChatMediator mediator;
    private User user1, user2, user3, user4;
    private ByteArrayOutputStream outContent;

    @Before
    public void setUp() {
        mediator = new ChatMediatorImpl();

        user1 = new UserImpl(mediator, "Alice");
        user2 = new UserImpl(mediator, "Jão");
        user3 = new UserImpl(mediator, "Pedro");
        user4 = new UserImpl(mediator, "Diana");

        mediator.adicionarUsuario(user1);
        mediator.adicionarUsuario(user2);
        mediator.adicionarUsuario(user3);
        mediator.adicionarUsuario(user4);

        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testEnviarMensagem() {
        user1.enviar("Oi, pessoal!");
        String expectedOutput = "Alice envia: Oi, pessoal!\n" +
                "Jão recebe: Oi, pessoal!\n" +
                "Pedro recebe: Oi, pessoal!\n" +
                "Diana recebe: Oi, pessoal!\n";
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    public void testReceberMensagem() {
        user3.enviar("Oi, Alice!");
        String expectedOutput = "Pedro envia: Oi, Alice!\n" +
                "Alice recebe: Oi, Alice!\n" +
                "Jão recebe: Oi, Alice!\n" +
                "Diana recebe: Oi, Alice!\n";
        assertEquals(expectedOutput, outContent.toString());
    }
}

