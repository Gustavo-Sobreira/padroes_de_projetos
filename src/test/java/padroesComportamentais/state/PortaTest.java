package padroesComportamentais.state;

import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertEquals;

public class PortaTest {

    private Porta porta;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        porta = new Porta();
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testAbrirPortaFechada() {
        porta.abrir();
        assertEquals("Abrindo a porta.\n", outContent.toString());
    }

    @Test
    public void testAbrirPortaAberta() {
        porta.abrir();
        porta.abrir(); // Tentativa de abrir a porta que já está aberta
        assertEquals("Abrindo a porta.\nA porta já está aberta.\n", outContent.toString());
    }

    @Test
    public void testFecharPortaAberta() {
        porta.abrir();
        porta.fechar();
        assertEquals("Abrindo a porta.\nFechando a porta.\n", outContent.toString());
    }

    @Test
    public void testFecharPortaFechada() {
        porta.fechar(); // Tentativa de fechar a porta que já está fechada
        assertEquals("A porta já está fechada.\n", outContent.toString());
    }

    @Test
    public void testTrancarPortaFechada() {
        porta.trancar();
        assertEquals("Trancando a porta.\n", outContent.toString());
    }

    @Test
    public void testTrancarPortaAberta() {
        porta.abrir();
        porta.trancar(); // Tentativa de trancar a porta que está aberta
        assertEquals("Abrindo a porta.\nTrancando a porta.\nVocê não pode trancar a porta enquanto ela estiver aberta.\n", outContent.toString());
    }

    @Test
    public void testDestrancarPortaTrancada() {
        porta.trancar();
        porta.destrancar();
        assertEquals("Trancando a porta.\nDestrancando a porta.\n", outContent.toString());
    }

    @Test
    public void testDestrancarPortaFechada() {
        porta.destrancar(); // Tentativa de destrancar a porta que está fechada
        assertEquals("Você não pode destrancar a porta enquanto ela estiver fechada.\n", outContent.toString());
    }
}
