package padroesComportamentais.interpreter;

import org.junit.Test;
import static org.junit.Assert.*;

public class ContextoTest {

    @Test
    public void testGetComando() {
        Contexto contexto = new Contexto("PedidoPizza");
        assertEquals("PedidoPizza", contexto.getComando());
    }
}
