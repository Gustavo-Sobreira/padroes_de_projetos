package padroesComportamentais.interpreter;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ExpressaoHamburguerTest {

    @Test
    public void testInterpretar() {
        Cozinha cozinha = new Cozinha();
        ExpressaoHamburguer expressaoHamburguer = new ExpressaoHamburguer(cozinha);
        Contexto contexto = new Contexto("PedidoHamburguer");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        expressaoHamburguer.interpretar(contexto);

        assertEquals("Hambúrguer está sendo preparado.\n", outContent.toString());
    }

    @Test
    public void testInterpretarComandoInvalido() {
        Cozinha cozinha = new Cozinha();
        ExpressaoHamburguer expressaoHamburguer = new ExpressaoHamburguer(cozinha);
        Contexto contexto = new Contexto("PedidoInvalido");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        expressaoHamburguer.interpretar(contexto);

        assertEquals("", outContent.toString());
    }
}
