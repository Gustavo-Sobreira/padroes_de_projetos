package padroesComportamentais.interpreter;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class ClienteTest {

    @Test
    public void testMain() {
        Cozinha cozinha = new Cozinha();

        List<Expressao> expressoes = new ArrayList<>();
        expressoes.add(new ExpressaoPizza(cozinha));
        expressoes.add(new ExpressaoSushi(cozinha));
        expressoes.add(new ExpressaoHamburguer(cozinha));

        String[] comandos = {"PedidoPizza", "PedidoSushi", "PedidoHamburguer"};

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        for (String comando : comandos) {
            Contexto contexto = new Contexto(comando);
            for (Expressao expressao : expressoes) {
                expressao.interpretar(contexto);
            }
        }

        String expectedOutput = "Pizza está sendo preparada.\n" +
                "Sushi está sendo preparado.\n" +
                "Hambúrguer está sendo preparado.\n";
        assertEquals(expectedOutput, outContent.toString());
    }
}
