package padroesComportamentais.interpreter;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class CozinhaTest {

    @Test
    public void testPrepararPizza() {
        Cozinha cozinha = new Cozinha();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        cozinha.prepararPizza();

        assertEquals("Pizza está sendo preparada.\n", outContent.toString());
    }

    @Test
    public void testPrepararSushi() {
        Cozinha cozinha = new Cozinha();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        cozinha.prepararSushi();

        assertEquals("Sushi está sendo preparado.\n", outContent.toString());
    }

    @Test
    public void testPrepararHamburguer() {
        Cozinha cozinha = new Cozinha();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        cozinha.prepararHamburguer();

        assertEquals("Hambúrguer está sendo preparado.\n", outContent.toString());
    }
}
