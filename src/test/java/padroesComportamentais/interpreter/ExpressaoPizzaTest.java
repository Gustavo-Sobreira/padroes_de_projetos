package padroesComportamentais.interpreter;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ExpressaoPizzaTest
{

    @Test
    public void testInterpretar() {
        Cozinha cozinha = new Cozinha();
        ExpressaoPizza expressaoPizza = new ExpressaoPizza(cozinha);
        Contexto contexto = new Contexto("PedidoPizza");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        expressaoPizza.interpretar(contexto);

        assertEquals("Pizza está sendo preparada.\n", outContent.toString());
    }

    @Test
    public void testInterpretarComandoInvalido() {
        Cozinha cozinha = new Cozinha();
        ExpressaoPizza expressaoPizza = new ExpressaoPizza(cozinha);
        Contexto contexto = new Contexto("PedidoInvalido");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        expressaoPizza.interpretar(contexto);

        assertEquals("", outContent.toString());
    }
}
