package padroesComportamentais.interpreter;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ExpressaoSushiTest {

    @Test
    public void testInterpretar() {
        Cozinha cozinha = new Cozinha();
        ExpressaoSushi expressaoSushi = new ExpressaoSushi(cozinha);
        Contexto contexto = new Contexto("PedidoSushi");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        expressaoSushi.interpretar(contexto);

        assertEquals("Sushi está sendo preparado.\n", outContent.toString());
    }

    @Test
    public void testInterpretarComandoInvalido() {
        Cozinha cozinha = new Cozinha();
        ExpressaoSushi expressaoSushi = new ExpressaoSushi(cozinha);
        Contexto contexto = new Contexto("PedidoInvalido");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        expressaoSushi.interpretar(contexto);

        assertEquals("", outContent.toString());
    }
}
