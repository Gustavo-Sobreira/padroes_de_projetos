package padroesComportamentais.iterator;

import java.util.List;

public class IteratorPorAno implements Iterator {
    private List<Livro> livros;
    private int ano;
    private int posicao;

    public IteratorPorAno(List<Livro> livros, int ano) {
        this.livros = livros;
        this.ano = ano;
        this.posicao = 0;
    }

    @Override
    public boolean hasNext() {
        while (posicao < livros.size()) {
            Livro livro = livros.get(posicao);
            if (livro.getAno() == ano) {
                return true;
            }
            posicao++;
        }
        return false;
    }

    @Override
    public Livro next() {
        return livros.get(posicao++);
    }
}
