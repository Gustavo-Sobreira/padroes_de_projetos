package padroesComportamentais.iterator;

import java.util.List;

public class IteratorPorAutor implements Iterator {
    private List<Livro> livros;
    private String autor;
    private int posicao;

    public IteratorPorAutor(List<Livro> livros, String autor) {
        this.livros = livros;
        this.autor = autor;
        this.posicao = 0;
    }

    @Override
    public boolean hasNext() {
        while (posicao < livros.size()) {
            Livro livro = livros.get(posicao);
            if (livro.getAutor().equalsIgnoreCase(autor)) {
                return true;
            }
            posicao++;
        }
        return false;
    }

    @Override
    public Livro next() {
        return livros.get(posicao++);
    }
}
