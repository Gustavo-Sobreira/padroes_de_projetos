package padroesComportamentais.iterator;

import java.util.List;

public class IteratorTodosLivros implements Iterator {
    private List<Livro> livros;
    private int posicao;

    public IteratorTodosLivros(List<Livro> livros) {
        this.livros = livros;
        this.posicao = 0;
    }

    @Override
    public boolean hasNext() {
        return posicao < livros.size();
    }

    @Override
    public Livro next() {
        return livros.get(posicao++);
    }
}
