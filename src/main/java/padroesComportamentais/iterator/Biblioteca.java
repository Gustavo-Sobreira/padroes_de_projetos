package padroesComportamentais.iterator;
import java.util.ArrayList;
import java.util.List;

public class Biblioteca {
    private List<Livro> livros;

    public Biblioteca() {
        this.livros = new ArrayList<>();
    }

    public void adicionarLivro(Livro livro) {
        livros.add(livro);
    }

    public Iterator criarIteratorTodosLivros() {
        return new IteratorTodosLivros(livros);
    }

    public Iterator criarIteratorPorAutor(String autor) {
        return new IteratorPorAutor(livros, autor);
    }

    public Iterator criarIteratorPorAno(int ano) {
        return new IteratorPorAno(livros, ano);
    }
}
