package padroesComportamentais.iterator;

public interface Iterator {
    boolean hasNext();
    Livro next();
}
