package padroesComportamentais.visitor;

public class FuncionarioTemporario implements Funcionario {
    private String nome;
    private int horasTrabalhadas;
    private double taxaHora;

    public FuncionarioTemporario(String nome, int horasTrabalhadas, double taxaHora) {
        this.nome = nome;
        this.horasTrabalhadas = horasTrabalhadas;
        this.taxaHora = taxaHora;
    }

    public String getNome() {
        return nome;
    }

    public int getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public double getTaxaHora() {
        return taxaHora;
    }

    @Override
    public void aceitar(Visitor visitor) {
        visitor.visitarFuncionarioTemporario(this);
    }
}

