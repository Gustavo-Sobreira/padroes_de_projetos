package padroesComportamentais.visitor;

public interface Visitor {
    void visitarFuncionarioPermanente(FuncionarioPermanente funcionario);
    void visitarFuncionarioTemporario(FuncionarioTemporario funcionario);
    void visitarEstagiario(Estagiario estagiario);
}
