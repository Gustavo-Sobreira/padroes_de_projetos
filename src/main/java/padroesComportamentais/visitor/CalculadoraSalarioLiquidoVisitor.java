package padroesComportamentais.visitor;

public class CalculadoraSalarioLiquidoVisitor implements Visitor {

    @Override
    public void visitarFuncionarioPermanente(FuncionarioPermanente funcionario) {
        System.out.println("Salário líquido de " + funcionario.getNome() + ": " + funcionario.getSalario());
    }

    @Override
    public void visitarFuncionarioTemporario(FuncionarioTemporario funcionario) {
        double salarioLiquido = funcionario.getTaxaHora() * funcionario.getHorasTrabalhadas();
        System.out.println("Salário líquido de " + funcionario.getNome() + ": " + salarioLiquido);
    }

    @Override
    public void visitarEstagiario(Estagiario estagiario) {
        System.out.println("Salário líquido de " + estagiario.getNome() + ": 0");
    }
}