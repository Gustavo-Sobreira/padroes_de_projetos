package padroesComportamentais.visitor;

public interface Funcionario {
    void aceitar(Visitor visitor);
}
