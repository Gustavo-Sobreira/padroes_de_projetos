package padroesComportamentais.visitor;

public class Estagiario implements Funcionario {
    private String nome;
    private int horasTrabalhadas;

    public Estagiario(String nome, int horasTrabalhadas) {
        this.nome = nome;
        this.horasTrabalhadas = horasTrabalhadas;
    }

    public String getNome() {
        return nome;
    }

    public int getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    @Override
    public void aceitar(Visitor visitor) {
        visitor.visitarEstagiario(this);
    }
}