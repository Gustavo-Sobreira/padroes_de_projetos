package padroesComportamentais.visitor;


public class CalculadoraHorasTrabalhadasVisitor implements Visitor {

    @Override
    public void visitarFuncionarioPermanente(FuncionarioPermanente funcionario) {
        System.out.println("Horas trabalhadas de " + funcionario.getNome() + ": 40 horas");
    }

    @Override
    public void visitarFuncionarioTemporario(FuncionarioTemporario funcionario) {
        System.out.println("Horas trabalhadas de " + funcionario.getNome() + ": " + funcionario.getHorasTrabalhadas() + " horas");
    }

    @Override
    public void visitarEstagiario(Estagiario estagiario) {
        System.out.println("Horas trabalhadas de " + estagiario.getNome() + ": " + estagiario.getHorasTrabalhadas() + " horas");
    }
}
