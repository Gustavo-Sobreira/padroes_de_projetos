package padroesComportamentais.visitor;

public class FuncionarioPermanente implements Funcionario {
    private String nome;
    private double salario;

    public FuncionarioPermanente(String nome, double salario) {
        this.nome = nome;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public double getSalario() {
        return salario;
    }

    @Override
    public void aceitar(Visitor visitor) {
        visitor.visitarFuncionarioPermanente(this);
    }
}