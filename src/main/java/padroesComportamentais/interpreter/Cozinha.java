package padroesComportamentais.interpreter;

public class Cozinha {
    public void prepararPizza() {
        System.out.println("Pizza está sendo preparada.");
    }

    public void prepararSushi() {
        System.out.println("Sushi está sendo preparado.");
    }

    public void prepararHamburguer() {
        System.out.println("Hambúrguer está sendo preparado.");
    }
}