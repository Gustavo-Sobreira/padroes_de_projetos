package padroesComportamentais.interpreter;

public class ExpressaoHamburguer implements Expressao {
    private Cozinha cozinha;

    public ExpressaoHamburguer(Cozinha cozinha) {
        this.cozinha = cozinha;
    }

    @Override
    public void interpretar(Contexto contexto) {
        if ("PedidoHamburguer".equalsIgnoreCase(contexto.getComando())) {
            cozinha.prepararHamburguer();
        }
    }
}