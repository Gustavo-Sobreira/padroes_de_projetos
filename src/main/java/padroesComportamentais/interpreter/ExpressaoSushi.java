package padroesComportamentais.interpreter;

public class ExpressaoSushi implements Expressao {
    private Cozinha cozinha;

    public ExpressaoSushi(Cozinha cozinha) {
        this.cozinha = cozinha;
    }

    @Override
    public void interpretar(Contexto contexto) {
        if ("PedidoSushi".equalsIgnoreCase(contexto.getComando())) {
            cozinha.prepararSushi();
        }
    }
}