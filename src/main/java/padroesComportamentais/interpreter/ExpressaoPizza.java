package padroesComportamentais.interpreter;

public class ExpressaoPizza implements Expressao {
    private Cozinha cozinha;

    public ExpressaoPizza(Cozinha cozinha) {
        this.cozinha = cozinha;
    }

    @Override
    public void interpretar(Contexto contexto) {
        if ("PedidoPizza".equalsIgnoreCase(contexto.getComando())) {
            cozinha.prepararPizza();
        }
    }
}