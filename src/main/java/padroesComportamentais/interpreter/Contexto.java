package padroesComportamentais.interpreter;

public class Contexto {
    private String comando;

    public Contexto(String comando) {
        this.comando = comando;
    }

    public String getComando() {
        return comando;
    }
}