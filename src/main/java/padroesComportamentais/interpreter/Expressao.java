package padroesComportamentais.interpreter;

public interface Expressao {
    void interpretar(Contexto contexto);
}