package padroesComportamentais.strategy;

public class PagamentoTransferenciaBancaria implements PagamentoStrategy {
    @Override
    public void processarPagamento(double valor) {
        System.out.println("Pagamento de R$" + valor + " realizado por transferência bancária.");
    }
}
