package padroesComportamentais.strategy;

public interface PagamentoStrategy {
    void processarPagamento(double valor);
}
