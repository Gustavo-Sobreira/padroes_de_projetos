package padroesComportamentais.strategy;

public class ProcessadorPagamento {
    private PagamentoStrategy estrategia;

    public void setEstrategia(PagamentoStrategy estrategia) {
        this.estrategia = estrategia;
    }

    public void processarPagamento(double valor) {
        estrategia.processarPagamento(valor);
    }
}
