package padroesComportamentais.memento;

public class Configuracoes {
    private String temaAtual;
    private String fonteAtual;
    private String corAtual;
    private Caretaker caretaker;

    public Configuracoes() {
        caretaker = new Caretaker();
    }

    public void personalizar(String tema, String fonte, String cor) {
        caretaker.salvarEstado(new Memento(temaAtual, fonteAtual, corAtual));
        temaAtual = tema;
        fonteAtual = fonte;
        corAtual = cor;
    }

    public void desfazer() {
        Memento estadoAnterior = caretaker.restaurarEstado();
        if (estadoAnterior != null) {
            temaAtual = estadoAnterior.getTemaSalvo();
            fonteAtual = estadoAnterior.getFonteSalva();
            corAtual = estadoAnterior.getCorSalva();
        }
    }

    public String getTemaAtual() {
        return temaAtual;
    }

    public String getFonteAtual() {
        return fonteAtual;
    }

    public String getCorAtual() {
        return corAtual;
    }
}
