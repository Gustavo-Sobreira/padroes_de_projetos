package padroesComportamentais.memento;

public class Memento {
    private String tema;
    private String fonte;
    private String cor;

    public Memento(String tema, String fonte, String cor) {
        this.tema = tema;
        this.fonte = fonte;
        this.cor = cor;
    }

    public String getTemaSalvo() {
        return tema;
    }

    public String getFonteSalva() {
        return fonte;
    }

    public String getCorSalva() {
        return cor;
    }
}
