package padroesComportamentais.memento;

import java.util.Stack;

public class Caretaker {
    private Stack<Memento> estados;

    public Caretaker() {
        estados = new Stack<>();
    }

    public void salvarEstado(Memento estado) {
        estados.push(estado);
    }

    public Memento restaurarEstado() {
        if (!estados.isEmpty()) {
            return estados.pop();
        }
        return null;
    }
}
