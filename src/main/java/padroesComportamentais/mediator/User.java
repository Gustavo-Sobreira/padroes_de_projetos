package padroesComportamentais.mediator;

public abstract class User {
    protected ChatMediator mediator;
    protected String nome;

    public User(ChatMediator mediator, String nome) {
        this.mediator = mediator;
        this.nome = nome;
    }

    public abstract void enviar(String mensagem);
    public abstract void receber(String mensagem);
}
