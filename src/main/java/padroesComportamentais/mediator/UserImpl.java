package padroesComportamentais.mediator;

public class UserImpl extends User {

    public UserImpl(ChatMediator mediator, String nome) {
        super(mediator, nome);
    }

    @Override
    public void enviar(String mensagem) {
        System.out.println(this.nome + " envia: " + mensagem);
        mediator.enviarMensagem(mensagem, this);
    }

    @Override
    public void receber(String mensagem) {
        System.out.println(this.nome + " recebe: " + mensagem);
    }
}
