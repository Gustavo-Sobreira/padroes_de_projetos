package padroesComportamentais.mediator;

import java.util.List;

public interface ChatMediator {
    void enviarMensagem(String mensagem, User usuario);
    void adicionarUsuario(User usuario);
}
