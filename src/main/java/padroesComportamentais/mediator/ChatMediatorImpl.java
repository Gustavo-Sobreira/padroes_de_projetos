package padroesComportamentais.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediatorImpl implements ChatMediator {
    private List<User> usuarios;

    public ChatMediatorImpl() {
        this.usuarios = new ArrayList<>();
    }

    @Override
    public void enviarMensagem(String mensagem, User usuario) {
        for (User u : usuarios) {
            if (u != usuario) {
                u.receber(mensagem);
            }
        }
    }

    @Override
    public void adicionarUsuario(User usuario) {
        this.usuarios.add(usuario);
    }
}
