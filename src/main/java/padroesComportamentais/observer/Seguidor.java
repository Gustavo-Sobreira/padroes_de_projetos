package padroesComportamentais.observer;

public class Seguidor implements Observer {
    private String nome;

    public Seguidor(String nome) {
        this.nome = nome;
    }

    @Override
    public void receberNotificacao(String mensagem) {
        System.out.println(nome + " recebeu uma notificação: " + mensagem);
    }
}
