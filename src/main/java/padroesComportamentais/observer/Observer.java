package padroesComportamentais.observer;

public interface Observer {
    void receberNotificacao(String mensagem);
}
