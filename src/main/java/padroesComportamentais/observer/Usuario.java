package padroesComportamentais.observer;

import java.util.ArrayList;
import java.util.List;

public class Usuario implements Subject {
    private String nome;
    private List<Observer> seguidores;

    public Usuario(String nome) {
        this.nome = nome;
        seguidores = new ArrayList<>();
    }

    @Override
    public void adicionarSeguidor(Observer seguidor) {
        seguidores.add(seguidor);
    }

    @Override
    public void removerSeguidor(Observer seguidor) {
        seguidores.remove(seguidor);
    }

    @Override
    public void notificarSeguidores(String mensagem) {
        for (Observer seguidor : seguidores) {
            seguidor.receberNotificacao(nome + ": " + mensagem);
        }
    }

    public void novaPostagem(String mensagem) {
        System.out.println(nome + " postou: " + mensagem);
        notificarSeguidores(mensagem);
    }
}

