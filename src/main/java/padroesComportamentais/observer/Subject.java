package padroesComportamentais.observer;

public interface Subject {
    void adicionarSeguidor(Observer seguidor);
    void removerSeguidor(Observer seguidor);
    void notificarSeguidores(String mensagem);
}
