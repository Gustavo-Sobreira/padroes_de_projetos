package padroesComportamentais.chainOfResponsability;

public class Tarefa {
    private TipoTarefa tipoTarefa;

    public Tarefa(TipoTarefa tipoTarefa) {
        this.tipoTarefa = tipoTarefa;
    }

    public TipoTarefa getTarefas() {
        return tipoTarefa;
    }

    public void setTipoTarefa(TipoTarefa tipoTarefa) {
        this.tipoTarefa = tipoTarefa;
    }
}