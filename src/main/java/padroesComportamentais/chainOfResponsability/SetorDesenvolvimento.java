package padroesComportamentais.chainOfResponsability;

public class SetorDesenvolvimento extends EsteiraProducao {
    public SetorDesenvolvimento(EsteiraProducao proximaEtapa) {
        listaDeTarefas.add(TipoTarefaDesenvolverCodigo.getTipoTarefa());
        setProximoSetor(proximaEtapa);
    }
    public String getEquipeRecebeDemanda() {
        return "Desenvolvimento";
    }
}
