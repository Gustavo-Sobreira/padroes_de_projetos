package padroesComportamentais.chainOfResponsability;

public class SetorAnalise extends EsteiraProducao {
    public SetorAnalise(EsteiraProducao proximaEtapa) {
        listaDeTarefas.add(TipoTarefaAnalisarDemanda.getTipoTarefa());
        setProximoSetor(proximaEtapa);
    }
    public String getEquipeRecebeDemanda() {
        return "Análise";
    }
}
