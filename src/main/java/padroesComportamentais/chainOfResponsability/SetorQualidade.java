package padroesComportamentais.chainOfResponsability;

public class SetorQualidade extends EsteiraProducao {
    public SetorQualidade(EsteiraProducao proximaEtapa) {
        listaDeTarefas.add(TipoTarefaTestarCodigo.getTipoTarefa());
        setProximoSetor(proximaEtapa);
    }
    public String getEquipeRecebeDemanda() {
        return "Teste";
    }
}
