package padroesComportamentais.chainOfResponsability;

public class TipoTarefaDesenvolverCodigo implements TipoTarefa{
    private static TipoTarefa desenvolverCodigo = new TipoTarefaDesenvolverCodigo();
    public static TipoTarefa getTipoTarefa() {
        return desenvolverCodigo;
    }
}
