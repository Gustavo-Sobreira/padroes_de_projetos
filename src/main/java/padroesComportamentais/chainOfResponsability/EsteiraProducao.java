package padroesComportamentais.chainOfResponsability;

import java.util.ArrayList;

public abstract class EsteiraProducao {
    protected EsteiraProducao proximoSetor;

    protected ArrayList listaDeTarefas = new ArrayList();

    public EsteiraProducao EsteiraProducao(){
        return proximoSetor;
    }

    public void setProximoSetor(EsteiraProducao proximoSetor){
        this.proximoSetor = proximoSetor;
    }

    public abstract String getEquipeRecebeDemanda();

    public String atribuirTarefa(Tarefa tarefa){
        if (listaDeTarefas.contains(tarefa.getTarefas())){
            return getEquipeRecebeDemanda();
        } else if (proximoSetor != null) {
            return  proximoSetor.atribuirTarefa(tarefa);
        }
        return "Nenhum setor encontrado";
    }
}
