package padroesComportamentais.templateMethod;

public class ConstrutorCasaAlvenaria extends ConstrutorCasa {

    @Override
    protected void construirEstrutura() {
        System.out.println("Construindo estrutura de alvenaria");
    }

    @Override
    protected void adicionarParedes() {
        System.out.println("Adicionando paredes de alvenaria");
    }

    @Override
    protected void adicionarTelhado() {
        System.out.println("Adicionando telhado de alvenaria");
    }

    @Override
    protected void adicionarPortasEJanelas() {
        System.out.println("Adicionando portas e janelas de alvenaria");
    }

    @Override
    protected void decorarCasa() {
        System.out.println("Decorando casa de alvenaria");
    }
}