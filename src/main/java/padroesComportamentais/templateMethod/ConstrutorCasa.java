package padroesComportamentais.templateMethod;

public abstract class ConstrutorCasa {

    public final void construirCasa() {
        construirEstrutura();
        adicionarParedes();
        adicionarTelhado();
        adicionarPortasEJanelas();
        decorarCasa();
    }

    protected abstract void construirEstrutura();
    protected abstract void adicionarParedes();
    protected abstract void adicionarTelhado();
    protected abstract void adicionarPortasEJanelas();
    protected abstract void decorarCasa();
}

