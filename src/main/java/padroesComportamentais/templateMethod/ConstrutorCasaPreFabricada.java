package padroesComportamentais.templateMethod;

public class ConstrutorCasaPreFabricada extends ConstrutorCasa {

    @Override
    protected void construirEstrutura() {
        System.out.println("Construindo estrutura pré-fabricada");
    }

    @Override
    protected void adicionarParedes() {
        System.out.println("Adicionando paredes pré-fabricadas");
    }

    @Override
    protected void adicionarTelhado() {
        System.out.println("Adicionando telhado pré-fabricado");
    }

    @Override
    protected void adicionarPortasEJanelas() {
        System.out.println("Adicionando portas e janelas pré-fabricadas");
    }

    @Override
    protected void decorarCasa() {
        System.out.println("Decorando casa pré-fabricada");
    }
}

