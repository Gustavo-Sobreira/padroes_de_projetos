package padroesComportamentais.templateMethod;

public class ConstrutorCasaMadeira extends ConstrutorCasa {

    @Override
    protected void construirEstrutura() {
        System.out.println("Construindo estrutura de madeira");
    }

    @Override
    protected void adicionarParedes() {
        System.out.println("Adicionando paredes de madeira");
    }

    @Override
    protected void adicionarTelhado() {
        System.out.println("Adicionando telhado de madeira");
    }

    @Override
    protected void adicionarPortasEJanelas() {
        System.out.println("Adicionando portas e janelas de madeira");
    }

    @Override
    protected void decorarCasa() {
        System.out.println("Decorando casa de madeira");
    }
}