package padroesComportamentais.command;

public class Tarefa {

    IBotao botao;

    Tarefa(IBotao botao){
        this.botao = botao;
    }

    public void executar(){
        this.botao.executar();
    }
}
