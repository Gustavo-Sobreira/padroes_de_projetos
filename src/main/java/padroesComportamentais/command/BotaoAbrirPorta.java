package padroesComportamentais.command;

public class BotaoAbrirPorta implements IBotao {
    @Override
    public void executar() {
        System.out.printf("Porta aberta");
    }
}
