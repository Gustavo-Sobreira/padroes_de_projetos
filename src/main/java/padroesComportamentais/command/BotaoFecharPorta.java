package padroesComportamentais.command;

public class BotaoFecharPorta implements IBotao {
    @Override
    public void executar() {
        System.out.printf("Porta fechada");
    }
}
