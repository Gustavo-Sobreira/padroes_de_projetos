package padroesComportamentais.state;

public class PortaFechada implements State {
    private final Porta porta;

    public PortaFechada(Porta porta) {
        this.porta = porta;
    }

    @Override
    public void abrir() {
        System.out.println("Abrindo a porta.");
        porta.mudarEstado(new PortaAberta(porta));
    }

    @Override
    public void fechar() {
        System.out.println("A porta já está fechada.");
    }

    @Override
    public void trancar() {
        System.out.println("Trancando a porta.");
        porta.mudarEstado(new PortaTrancada(porta));
    }

    @Override
    public void destrancar() {
        System.out.println("Você não pode destrancar a porta enquanto ela estiver fechada.");
    }
}