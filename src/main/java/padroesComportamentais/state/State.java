package padroesComportamentais.state;

public interface State {
    void abrir();
    void fechar();
    void trancar();
    void destrancar();
}
