package padroesComportamentais.state;

public class PortaAberta implements State {
    private final Porta porta;

    public PortaAberta(Porta porta) {
        this.porta = porta;
    }

    @Override
    public void abrir() {
        System.out.println("A porta já está aberta.");
    }

    @Override
    public void fechar() {
        System.out.println("Fechando a porta.");
        porta.mudarEstado(new PortaFechada(porta));
    }

    @Override
    public void trancar() {
        System.out.println("Você não pode trancar a porta enquanto ela estiver aberta.");
    }

    @Override
    public void destrancar() {
        System.out.println("A porta já está destrancada.");
    }
}




