package padroesComportamentais.state;

public class PortaTrancada implements State {
    private final Porta porta;

    public PortaTrancada(Porta porta) {
        this.porta = porta;
    }

    @Override
    public void abrir() {
        System.out.println("A porta está trancada. Você não pode abri-la.");
    }

    @Override
    public void fechar() {
        System.out.println("A porta está trancada. Você não pode fechá-la.");
    }

    @Override
    public void trancar() {
        System.out.println("A porta já está trancada.");
    }

    @Override
    public void destrancar() {
        System.out.println("Destrancando a porta.");
        porta.mudarEstado(new PortaFechada(porta));
    }
}