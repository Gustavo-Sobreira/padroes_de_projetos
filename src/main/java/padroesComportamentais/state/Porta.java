package padroesComportamentais.state;

public class Porta {
    private State estadoAtual;

    public Porta() {
        this.estadoAtual = new PortaFechada(this);
    }

    public void mudarEstado(State novoEstado) {
        this.estadoAtual = novoEstado;
    }

    public void abrir() {
        estadoAtual.abrir();
    }

    public void fechar() {
        estadoAtual.fechar();
    }

    public void trancar() {
        estadoAtual.trancar();
    }

    public void destrancar() {
        estadoAtual.destrancar();
    }
}
