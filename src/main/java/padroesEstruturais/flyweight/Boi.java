package padroesEstruturais.flyweight;

public interface Boi {
    void mostrarDetalhes(String detalhesExtras);
}
