package padroesCriacao.abstractFactory;

public interface Carro {

    String acelerar();

    String frear();
}