package padroesCriacao.abstractFactory;

public interface Moto {

    String acelerar();

    String frear();
}
