package padroesCriacao.abstractFactory;

public interface MontadoraFactory {
    Carro montarCarro();
    Moto montarMoto();
}
