<h1 align="center">COMMAND</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Encapsular uma solicitação como um objeto, desta forma permitindo parametrizar
clientes com diferentes solicitações, enfileirar ou fazer o registro (log) de solicitações
e suportar operações que podem ser desfeitas." (Gamma,2000, p.222).

# ⚠️ Problema
Crie uma solução que seja capaz de gerenciar uma casa inteligente.
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.command**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.command**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








