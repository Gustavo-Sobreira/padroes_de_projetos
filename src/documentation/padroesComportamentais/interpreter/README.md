<h1 align="center">INTERPRETER</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Dada uma linguagem, definir uma representação para a sua gramática juntamente
com um interpretador que usa a representação para interpretar sentenças dessa
linguagem." (Gamma,2000, p.231).

# ⚠️ Problema
Você está desenvolvendo um sistema de controle de pedidos para um restaurante, onde os clientes podem fazer pedidos para diferentes pratos (por exemplo, pizza, sushi, hambúrguer). Cada pedido precisa ser interpretado a partir de uma string de comando e, em seguida, executado, exibindo uma mensagem de confirmação.
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.interpreter**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.interpreter**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








