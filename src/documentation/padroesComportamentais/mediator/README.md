<h1 align="center">MEDIATOR</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Sem violar o encapsulamento, capturar e externalizar um estado interno de um
objeto, de maneira que o objeto possa ser restaurado para esse estado mais tarde." (Gamma,2000, p.266).

# ⚠️ Problema
Grupão do zap
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.mediator**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.mediator**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








