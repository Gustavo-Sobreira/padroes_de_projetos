<h1 align="center">MEMENTO</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Definir um objeto que encapsula a forma como um conjunto de objetos interage. O
Mediator promove o acoplamento fraco ao evitar que os objetos se refiram uns aos
outros explicitamente e permite variar suas interações independentemente." (Gamma,2000, p.257).

# ⚠️ Problema
Configurações de sistemas operacionais
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.mediator**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.mediator**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








