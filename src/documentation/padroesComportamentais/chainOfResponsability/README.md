<h1 align="center">CHAIN OF RESPONSIBILITY</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Devitar o acoplamento do remetente de uma solicitação ao seu receptor, ao dar a mais de
um objeto a oportunidade de tratar a solicitação. Encadear os objetos receptores,
passando a solicitação ao longo da cadeia até que um objeto a trate." (Gamma,2000, p.212).

# ⚠️ Problema
Crie um programa gerencie uma esteira de produção.
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.chainOfResponsability**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.chainOfResponsability**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








