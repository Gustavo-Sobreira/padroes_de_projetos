<h1 align="center">STRATEGY</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Definir uma família de algoritmos, encapsular cada uma delas e torná-las intercam-
biáveis. Strategy permite que o algoritmo varie independentemente dos clientes que
o utilizam." (Gamma,2000, p.292).

# ⚠️ Problema
Pagamneto eletrônico
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.observer**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.observer**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








