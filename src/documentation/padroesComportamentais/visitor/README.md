<h1 align="center">VISITOR</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Representar uma operação a ser executada nos elementos de uma estrutura de
objetos. Visitor permite definir uma nova operação sem mudar as classes dos
elementos sobre os quais opera." (Gamma,2000, p.305).

# ⚠️ Problema
Calcule os diferentes salários de uma empresa
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.observer**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.observer**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








