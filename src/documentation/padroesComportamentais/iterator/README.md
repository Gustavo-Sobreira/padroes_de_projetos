<h1 align="center">ITERATOR</h1>

***

### Escopo: Objetos
### Propósito: Comportamental
"Fornecer um meio de acessar, seqüencialmente, os elementos de um objeto agregado sem
expor a sua representação subjacente." (Gamma,2000, p.244).

# ⚠️ Problema
Você está desenvolvendo um sistema de biblioteca que gerencia uma coleção de livros. A biblioteca deseja implementar funcionalidades que permitam aos usuários percorrer a coleção de livros de várias maneiras, como por exemplo:

    Percorrer todos os livros da coleção.
    Percorrer apenas os livros de um determinado autor.
    Percorrer apenas os livros publicados em um determinado ano.


***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesComportamentais.iterator**
- Pacote de Testes: está localizado em: **src.test.java.padroesComportamentais.iterator**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








