<h1 align="center">Adapter</h1>

***

### Escopo: Classe
### Propósito: Estrutural
"Converter a interface de uma classe em outra interface, esperada pelos clientes. O
Adapter permite que classes com interfaces incompatíveis trabalhem em conjunto –
o que, de outra forma, seria impossível." (Gamma,2000, p.140).
***
![coelho.jpg](coelho.jpg)

# ⚠️ Problema
### Padronizar entregas de páscoa
A CacaUChiq confeitarias está com problemas na padronização do tamanho de seus ovos, pois trabalha com franquias e cada 
confeiteriro interpreta de um modo o que é um ovo médio, para isso a CacaUChiq crie uma solução que contorne esses 
problemas de padronização, onde:
  - Ovo grande...1000 g
  - Ovo médio......500 g
  - Ovo pequeno..250 g
***
# 📐 Modelo
![img.png](img.png)
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesEstruturais.adapter**
- Pacote de Testes: está localizado em: **src.test.java.padroesEstruturais.adapter**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








