<h1 align="center">Decorator</h1>

***

### Escopo: Objetos
### Propósito: Estrutural
"Dinamicamente, agregar responsabilidades adicionais a um objeto. Os Decorators
fornecem uma alternativa flexível ao uso de subclasses para extensão de funcionali-
dades." (Gamma,2000, p.170).

# ⚠️ Problema
### Padronizar entregas de páscoa
Crie no RPG que foi criado adicione armas para os personagens.
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesEstruturais.adapter**
- Pacote de Testes: está localizado em: **src.test.java.padroesEstruturais.adapter**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








