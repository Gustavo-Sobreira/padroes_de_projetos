<h1 align="center">Bridge</h1>

***

### Escopo: Objetos
### Propósito: Estrutural
"Desacoplar uma abstração da sua implementação, de modo que as duas possam variar
independentemente." (Gamma,2000, p.151).

# ⚠️ Problema
### Padronizar entregas de páscoa
Monte um RPG que deixe o jogador escolher qual classe de personagem que jogar, calcule sua defesa e ataque:
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesEstruturais.adapter**
- Pacote de Testes: está localizado em: **src.test.java.padroesEstruturais.adapter**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








