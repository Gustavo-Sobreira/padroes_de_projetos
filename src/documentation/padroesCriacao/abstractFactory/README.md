<h1 align="center">Abstract Factory</h1>

***


### Escopo: Objetos
### Propósito: Criação
"Fornecer uma interface para criação de famílias de objetos relacionados ou dependentes sem especificar suas classes concretas." (Gamma,2000, p.95).
***
# ⚠️ Problema
### Concessionária de Veículos

Uma concessionária de veículos, deve permitir o test-drive de seus veículos idependentemente da marca, modelo ou tipo de veículo que seja. Para isso crie uma solução utilizando o padrão Abstract Factory, permitindo o test-drive em todos veículos.
***
# 📐 Modelo
![img.png](img.png)
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesCriacao.abstractFactory**
- Pacote de Testes: está localizado em: **src.test.java.padroesCriacao.abstractFactory**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.







