<h1 align="center">Prototype</h1>

***


### Escopo: Objetos
### Propósito: Criação
"Especificar os tipos de objetos a serem criados usando uma instância-protótipo e criar
novos objetos pela cópia desse protótipo." (Gamma,2000, p.121).
***
# ⚠️ Problema
### Cadastrar novas espécies
Crie
- Crie um projeto que possa gerar arquivos de diferentes formatos e que possa ser simples de estender.
***
# 📐 Modelo
![img.png](img.png)
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesCriacao.factoryMethod**
- Pacote de Testes: está localizado em: **src.test.java.padroesCriacao.factoryMethod**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








