<h1 align="center">Factory Method</h1>

***


### Escopo: Objetos
### Propósito: Criação
"Definir uma interface para criar um objeto, mas deixar as subclasses decidirem que
classe instanciar. O Factory Method permite adiar a instanciação para subclasses." (Gamma,2000, p.112).
***
# ⚠️ Problema
### Cadastrar novas espécies
Crie
- Crie um projeto que possa gerar arquivos de diferentes formatos e que possa ser simples de estender.
***
# 📐 Modelo
![alt text](image.png)
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesCriacao.factoryMethod**
- Pacote de Testes: está localizado em: **src.test.java.padroesCriacao.factoryMethod**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








