CREATE DATABASE IF NOT EXISTS catalogo;

USE catalogo;

CREATE TABLE IF NOT EXISTS classificacao (
    id INT AUTO_INCREMENT PRIMARY KEY,
    reino VARCHAR(50),
    filo VARCHAR(50),
    classe VARCHAR(50),
    ordem VARCHAR(50),
    familia VARCHAR(50),
    genero VARCHAR(50),
    especie VARCHAR(50),
    nm_popular VARCHAR(50)
);

INSERT INTO classificacao (reino, filo, classe, ordem, familia, genero, especie, nm_popular)
VALUES
    ('Animalia', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'leo', 'Leao'),
    ('Animalia', 'Chordata', 'Mammalia', 'Primates', 'Hominidae', 'Homo', 'sapiens', 'Humano'),
    ('Animalia', 'Chordata', 'Aves', 'Falconiformes', 'Accipitridae', 'Haliaeetus', 'leucocephalus', 'Aguia-careca'),
    ('Animalia', 'Arthropoda', 'Insecta', 'Hymenoptera', 'Apidae', 'Tetragonisca', 'angustula', 'Abelha-jatai'),
    ('Animalia', 'Chordata', 'Mammalia', 'Rodentia', 'Muridae', 'Rattus', 'norvegicus', 'Rato');
