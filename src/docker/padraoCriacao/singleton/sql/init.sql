CREATE DATABASE singleton;

\c singleton

CREATE SCHEMA IF NOT EXISTS s_user;
ALTER SCHEMA s_user OWNER TO postgres;

CREATE TABLE IF NOT EXISTS s_user.cadastro (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(100),
    idade INT
);

INSERT INTO s_user.cadastro (nome, idade) VALUES
    ('João', 30),
    ('Maria', 25),
    ('José', 35),
    ('Ana', 28),
    ('Pedro', 32),
    ('Mariana', 27),
    ('Carlos', 40),
    ('Sandra', 33),
    ('Paulo', 29),
    ('Lúcia', 31);



